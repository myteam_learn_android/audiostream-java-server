package com.sebastian.utoiu;

import javax.sound.sampled.AudioFormat;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import java.io.*;

import javax.sound.sampled.*;


public class ServerSebi_2 {


    //private ServerStream.OutgoingSoudnListener osl = new ServerStream.OutgoingSoudnListener();
    boolean outVoice = true;
    AudioFormat format = getAudioFormat();

    private ServerSocket serverSocket;
    //Socket server; la sebi e socket

    private AudioFormat getAudioFormat() {
        float sampleRate = 16000.0F;
        int sampleSizeBits = 16;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = false;

        return new AudioFormat(sampleRate, sampleSizeBits, channels, signed, bigEndian);
    }

    public void startServer()
    {
        try (ServerSocket listener = new ServerSocket(59090)) {
            System.out.println("The server is running...");
            while (true) {
                try (Socket socket = listener.accept()) {

                    System.out.println("Listening from mic.");
                    DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                    DataLine.Info micInfo = new DataLine.Info(TargetDataLine.class, format);
                    TargetDataLine mic = (TargetDataLine) AudioSystem.getLine(micInfo);
                    mic.open(format);
                    System.out.println("Mic open.");
                    byte tmpBuff[] = new byte[mic.getBufferSize() / 5];
                    mic.start();
                    while (outVoice) {
                        System.out.println("Reading from mic.");
                        int count = mic.read(tmpBuff, 0, tmpBuff.length);
                        if (count > 0) {
                            System.out.println("Writing buffer to server.");
                            out.write(tmpBuff, 0, count);
                        }
                    }
                    mic.drain();
                    mic.close();
                    System.out.println("Stopped listening from mic.");


//                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
//                    InputStream inputStream = socket.getInputStream();
//                    DataInputStream dataInputStream = new DataInputStream(inputStream);
//
//
//                    String message = dataInputStream.readUTF();
//                    String numbers[] = message.split(" ");
//                    int suma = Integer.valueOf(numbers[0])+Integer.valueOf(numbers[1]);
//                    out.println(String.valueOf(suma));
                }
            }
        }catch (Exception e)
        {
            System.out.println("Exceptie "+e.getMessage());
        }
    }

}
