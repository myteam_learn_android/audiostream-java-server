package com.sebastian.utoiu;

import java.awt.Button;
import java.awt.Frame;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataOutputStream;
import java.util.Scanner;
import java.net.Socket;
import java.io.IOException;


public class ClientSebi extends Frame implements ActionListener {
    TextField tf1, tf2, tf3;
    Button b1, b2;

    ClientSebi() {
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        tf1 = new TextField();
        tf1.setBounds(50, 50, 150, 20);
        tf2 = new TextField();
        tf2.setBounds(50, 100, 150, 20);
        tf3 = new TextField();
        tf3.setBounds(50, 150, 150, 20);
        tf3.setEditable(false);
        b1 = new Button("+");
        b1.setBounds(50, 200, 50, 50);
        b1.addActionListener(this);
        add(tf1);
        add(tf2);
        add(tf3);
        add(b1);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);


    }

    public void actionPerformed(ActionEvent e) {
        String s1 = tf1.getText();
        String s2 = tf2.getText();
        int a = Integer.parseInt(s1);
        int b = Integer.parseInt(s2);

        try {
            Socket socket = new Socket("localhost", 59090);

            Scanner scanner = new Scanner(System.in);
            Scanner in = new Scanner(socket.getInputStream());

            DataOutputStream data = new DataOutputStream(socket.getOutputStream());
            data.writeUTF(s1 + " " + s2);
            data.flush();


            tf3.setText(in.nextLine());
            data.close();

        } catch (IOException ee) {

            System.out.println("Exceptie la client "+ee.getMessage());
        }

    }
    public static void main(String [] args) throws IOException{
        new ClientSebi();
    }

}
