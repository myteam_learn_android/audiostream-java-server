package com.sebastian.utoiu;

import org.apache.commons.io.IOUtils;

import javax.sound.sampled.*;
import java.io.*;
import java.net.*;
import java.util.*;



/**
 * This program demonstrates how to implement a UDP server program.
 *
 *
 * @author www.codejava.net
 */
public class QuoteServer {
    private DatagramSocket socket;
    private List<String> listQuotes = new ArrayList<String>();
    private Random random;

    public QuoteServer(int port) throws SocketException {
        socket = new DatagramSocket(port);
        random = new Random();
    }

    //private ClientStream.IncomingSoundListener isl = new ClientStream.IncomingSoundListener();
    //AudioFormat format = getAudioFormat();
    //InputStream is;
    //Socket client;
//    String serverName = "localhost";
//    int port = PORT_STREAM;
//    boolean inVoice = true;

//
//    private AudioFormat getAudioFormat() {
//        float sampleRate = 16000.0F;
//        int sampleSizeBits = 16;
//        int channels = 1;
//        boolean signed = true;
//        boolean bigEndian = false;
//
//        return new AudioFormat(sampleRate, sampleSizeBits, channels, signed, bigEndian);
//    }

    public static void main(String[] args) {


        String quoteFile = "quotes.txt";
        int port = 17;

        try {
            QuoteServer server = new QuoteServer(port);
            //server.loadQuotesFromFile(quoteFile);
            server.service();
        } catch (SocketException ex) {
            System.out.println("Socket error: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex.getMessage());
        }
    }

    private void service() throws IOException {
        while (true) {
            DatagramPacket request = new DatagramPacket(new byte[1], 1);
            socket.receive(request);

            ///-----
            //String quote = getRandomQuote();
            //byte[] buffer = quote.getBytes();
            //---------------------

            AudioFormat format = new AudioFormat(8000.0f, 16, 1, true, true);
            TargetDataLine microphone;
            //SourceDataLine speakers;
            try {
                microphone = AudioSystem.getTargetDataLine(format);

                DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
                microphone = (TargetDataLine) AudioSystem.getLine(info);
                microphone.open(format);

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                int numBytesRead;
                int CHUNK_SIZE = 1024;
                byte[] data = new byte[microphone.getBufferSize() / 5];
                microphone.start();

                int bytesRead = 0;
//                DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
//                speakers = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
//                speakers.open(format);
//                speakers.start();
                while (bytesRead < 10000) {
                    numBytesRead = microphone.read(data, 0, CHUNK_SIZE);
                    bytesRead += numBytesRead;
                    // write the mic data to a stream for use later
                    out.write(data, 0, numBytesRead);
                    // write mic data to stream for immediate playback
                    //speakers.write(data, 0, numBytesRead);


                    byte[] buffer = data;
                    InetAddress clientAddress = request.getAddress();
                    int clientPort = request.getPort();

                    DatagramPacket response = new DatagramPacket(buffer, buffer.length, clientAddress, clientPort);
                    socket.send(response);
                }
//                speakers.drain();
//                speakers.close();
                microphone.close();
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            }

            //end adaug eu
            //--------------


        }
    }

//    private void loadQuotesFromFile(String quoteFile) throws IOException {
//        BufferedReader reader = new BufferedReader(new FileReader(quoteFile));
//        String aQuote;
//
//        while ((aQuote = reader.readLine()) != null) {
//            listQuotes.add(aQuote);
//        }
//
//        reader.close();
//    }
//
//    private String getRandomQuote() {
//        int randomIndex = random.nextInt(listQuotes.size());
//        String randomQuote = listQuotes.get(randomIndex);
//        return randomQuote;
//    }
}