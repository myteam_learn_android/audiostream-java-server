package com.sebastian.utoiu;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * This program demonstrates how to implement a UDP server program.
 *
 *
 * @author www.codejava.net
 */
public class QuoteServerReceiveFromAndroidAndDisplay {
    private DatagramSocket socketReceiveFromAndroidRecorded;

    private ServerWhoSendsData threadServerShoSendsData;


//    private List<String> listQuotes = new ArrayList<String>();
//    private Random random;

    public QuoteServerReceiveFromAndroidAndDisplay(int portReceive, int portSend) throws SocketException {
        socketReceiveFromAndroidRecorded = new DatagramSocket(portReceive);
        threadServerShoSendsData = new ServerWhoSendsData(portSend);

        //random = new Random();
    }

    //private ClientStream.IncomingSoundListener isl = new ClientStream.IncomingSoundListener();
    //InputStream is;
    //Socket client;



    public static void main(String[] args) {


        String quoteFile = "quotes.txt";
        int portReceive = 17;
        int portSend = 12555;

        try {
            QuoteServerReceiveFromAndroidAndDisplay server = new QuoteServerReceiveFromAndroidAndDisplay(portReceive, portSend);
            //server.loadQuotesFromFile(quoteFile);
            server.service();
        } catch (SocketException ex) {
            System.out.println("Socket error: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex.getMessage());
        }
        catch (Throwable t)
        {
            System.out.println("Eroare plm" +t.getMessage());
        }
    }

    private void service() throws IOException {

        System.out.println("Server to get Android Audio is running");

        while (true) {

            System.out.println("11111");

            //asta e receive primul ping, in cazul in care imi trebuie adresa clientului, in cazul de fata a ghidului care vorbeste
            DatagramPacket request = new DatagramPacket(new byte[1], 1);
            socketReceiveFromAndroidRecorded.receive(request);
            System.out.println("11111 - s-a conectat ghidul");

            InetAddress clientAddressOfTheGuide = request.getAddress();

            //de aici incolo o sa incep sa primesc audio, si o sa le afisez la consola


            while (true) {

                byte[] buffer = new byte[6400];
                DatagramPacket response = new DatagramPacket(buffer, buffer.length);
                socketReceiveFromAndroidRecorded.receive(response);

                //  speakers.write(buffer, 0, response.getLength());

                threadServerShoSendsData.ceEDeAfisatLaUnMomentDat = buffer;
                threadServerShoSendsData.dimensiunea = response.getLength();
                threadServerShoSendsData.veziCamAmTrimis();

                //aici daca vreau sa le afisez ca log
//                String quote = new String(buffer, 0, response.getLength());
//                System.out.println(quote);
//                System.out.println();


                //---aici trebuie sa pornesc un thread cu server 2

            }

            //end adaug eu
            //--------------


        }
    }


}

class ServerWhoSendsData extends Thread{
    private DatagramSocket socketSendToAnotherAndroid;

    //in astea pushuie serverul care primeste
    public byte[] ceEDeAfisatLaUnMomentDat;
    public int dimensiunea;

    public boolean isSomeoneConnected = false;

    InetAddress clientAddress;
    int clientPort ;

    //o comentez sa nu incarc serverul
    public void veziCamAmTrimis()
    {
//        String quote = new String(ceEDeAfisatLaUnMomentDat, 0, dimensiunea);
//        System.out.println(quote);
//        System.out.println();

        if (isSomeoneConnected) {
            try {
                //aici trimit la clientul care se contecteaza, ce am in momentul de fata
                //DatagramPacket response = new DatagramPacket(ceEDeAfisatLaUnMomentDat, ceEDeAfisatLaUnMomentDat.length, clientAddress, clientPort);
                DatagramPacket response = new DatagramPacket(ceEDeAfisatLaUnMomentDat, dimensiunea, clientAddress, clientPort);
                socketSendToAnotherAndroid.send(response);
            } catch (Exception e) {
                System.out.println("problemm");
            }
        }
    }

    @Override
    public void run() {


        System.out.println("Serverul de trimis Android audio e si el in picioare");

        //aici scriu codul de server

        try {

            while (true) {

                System.out.println("2222");
                DatagramPacket request = new DatagramPacket(new byte[1], 1);
                socketSendToAnotherAndroid.receive(request); //aici primeste un ping ca sa stie cui sa trimita inapoi


                System.out.println("2222--s-a conectat turistul");


                isSomeoneConnected = true;

//            String quote = getRandomQuote();
//            byte[] buffer = quote.getBytes();

                clientAddress = request.getAddress();
                clientPort = request.getPort();

                break;

//
//                while (true) {
//                    //aici trimit la clientul care se contecteaza, ce am in momentul de fata
//                    DatagramPacket response = new DatagramPacket(ceEDeAfisatLaUnMomentDat, ceEDeAfisatLaUnMomentDat.length, clientAddress, clientPort);
//                    socketSendToAnotherAndroid.send(response);
//
//                }
            }
        } catch (Exception e)
        {
            System.out.println("A crapat serverul de send");

        }


    }

    public ServerWhoSendsData(int portSend)
    {
        try {
            socketSendToAnotherAndroid = new DatagramSocket(portSend);
            start();
        }catch (Exception e)
        {
            System.out.println("Nu reuseste sa faca socketul de send");
        }
    }





}