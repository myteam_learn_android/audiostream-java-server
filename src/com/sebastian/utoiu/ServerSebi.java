package com.sebastian.utoiu;

import java.awt.im.InputContext;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
public class ServerSebi {

    public void startServer()
    {
        try (ServerSocket listener = new ServerSocket(59090)) {
            System.out.println("The server is running...");
            while (true) {
                try (Socket socket = listener.accept()) {
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    InputStream inputStream = socket.getInputStream();
                    DataInputStream dataInputStream = new DataInputStream(inputStream);


                    String message = dataInputStream.readUTF();
                    String numbers[] = message.split(" ");
                    int suma = Integer.valueOf(numbers[0])+Integer.valueOf(numbers[1]);
                    out.println(String.valueOf(suma));
                }
            }
        }catch (Exception e)
        {
            System.out.println("Exceptie "+e.getMessage());
        }
    }

    public static void main(String [] args) throws IOException{
        ServerSebi s = new ServerSebi();
        s.startServer();
    }
}
