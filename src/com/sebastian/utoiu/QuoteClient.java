package com.sebastian.utoiu;

import java.io.*;
import java.net.*;
import javax.sound.sampled.*;


/**
 * This program demonstrates how to implement a UDP client program.
 *
 *
 * @author www.codejava.net
 */
public class QuoteClient {

    public static void main(String[] args) {


//        String hostname = "djxmmx.net";
//        int port = 17;

        String hostname = "localhost";
        int port = 17;

        AudioFormat format = new AudioFormat(8000.0f, 16, 1, true, true);
        SourceDataLine speakers;


        try {
            //open speakers
            DatagramSocket socket = new DatagramSocket();

            DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
            speakers = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
            speakers.open(format);
            speakers.start();

            //send a ping to the server
            InetAddress address = InetAddress.getByName(hostname);
            //fac un request sa trezesc serverul
            DatagramPacket request = new DatagramPacket(new byte[1], 1, address, port);
            socket.send(request);

            while (true) {

                byte[] buffer = new byte[1024];
                DatagramPacket response = new DatagramPacket(buffer, buffer.length);
                socket.receive(response);


                // write reveived(microphone) data to stream for immediate playback
                speakers.write(buffer, 0, response.getLength());



//                String quote = new String(buffer, 0, response.getLength());
//
//                System.out.println(quote);
//                System.out.println();

                //Thread.sleep(10000);
            }

//                speakers.drain();
//                speakers.close();

        } catch (SocketTimeoutException ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Client error: " + ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}